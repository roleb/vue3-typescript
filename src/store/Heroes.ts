import store from "./index";
import { Module, VuexModule, Mutation, Action } from "vuex-module-decorators";
import Hero from "../types/Hero";
const URL = `https://reqres.in/api/users?page2`;
@Module({ name: "Heroes", dynamic: true, store })
export default class Heroes extends VuexModule {
  heroes: Hero[] = [];
  editedHero: Hero = {
    id: 0,
    first_name: "",
    last_name: "",
    email: "",
    avatar: "",
  };
  get GetHeroes(): Hero[] {
    return this.heroes;
  }
  get GetEditedHero(): Hero {
    return this.editedHero;
  }

  @Mutation
  SET_HEROES(heroes: Hero[]) {
    this.heroes = [...heroes];
  }

  @Mutation
  SET_EDITED_HERO(hero: Hero) {
    this.editedHero = { ...hero };
  }

  @Action
  async SetHeroes() {
    const response = await fetch(URL);
    const { data } = await response.json();
    const { ok, status } = await response;
    return ok ? this.SET_HEROES(data) : status;
  }
}
